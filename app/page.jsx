import Hero from "@/components/Hero"
import InfoBoxes from "@/components/InfoBoxes"
import HomeProperties from "@/components/HomeProperties"
import connectDB from "@/config/database";

const Homepage = async  () => {
await connectDB();
//console.log(process.env.MONGODB_URI);


  return (
    <div>
      <Hero />
      <InfoBoxes />
      <HomeProperties />
   
    </div>
  )
}

export default Homepage