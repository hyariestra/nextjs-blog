import React from "react";
import InfoBox from "./InfoBox";
const InfoBoxes = () => {
  return (
    <section>
      <div className="container-xl lg:container m-auto">
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4 p-4 rounded-lg">
          <InfoBox
            heading="For Renters"
            backgoundColor="bg-gray-100"
            buttonInfo={{
              text: "browse properties",
              link: "/properties",
              backgoundColor: "bg-black",
            }}
          >
          List your properties and reach potential tenants. Rent as an airbnb or
          long term.
          </InfoBox>
          <InfoBox
            heading="For Property owner "
            backgoundColor="bg-blue-100"
            buttonInfo={{
              text: "add properties",
              link: "/properties/add",
              backgoundColor: "bg-blue-500",
            }}
          >
          List your properties and reach potential tenants. Rent as an airbnb or
          long term.
          </InfoBox>
          
        </div>
      </div>
    </section>
  );
};

export default InfoBoxes;
